package com.password;
//file: Password.java

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class Password extends JPanel implements ActionListener {
	private static final long serialVersionUID = 1L;
	
	static JComboBox dayList = new JComboBox(GetInfo.getDayValues());
	static JLabel picture = new JLabel();
	static JLabel tPass = new JLabel();
	static JLabel nPass = new JLabel();
	static JLabel today = new JLabel();
	static JTextField newDate = new JTextField();
	static JTextField newYear = new JTextField();
	static JFrame frame = new JFrame("Master Password");
	
	public static void init(){
		dayList.setSelectedIndex(GetInfo.getDayValue());

		picture.setFont(picture.getFont().deriveFont(Font.ITALIC));
		updateLabel("Adv-Con.gif");
		picture.setBorder(BorderFactory.createEmptyBorder(0,28,35,0));
		picture.setPreferredSize(new Dimension(330, 141));

		today.setFont(today.getFont().deriveFont(Font.TRUETYPE_FONT));
		today.setHorizontalAlignment(JLabel.CENTER);
		today.setText(GetInfo.getDate());

		tPass.setFont(tPass.getFont().deriveFont(Font.BOLD));
		tPass.setHorizontalAlignment(JLabel.CENTER);
		tPass.setText("Today's Password is:  " + GetInfo.getPassword());
		
		newDate.setFont(newDate.getFont().deriveFont(Font.BOLD));
		newDate.setColumns(3);
		newDate.setHorizontalAlignment(JLabel.CENTER);
		newDate.setText(GetInfo.getDate(true));
		
		newYear.setFont(newYear.getFont().deriveFont(Font.BOLD));
		newYear.setColumns(5);
		newYear.setHorizontalAlignment(JLabel.CENTER);
		newYear.setText(GetInfo.getDate(false));
}
	
	public Password(){
		super(new BorderLayout());

		dayList.addActionListener(this);
		newDate.addActionListener(this);
		newYear.addActionListener(this);
		
		//Lay out the window
		JPanel labelPane = new JPanel();
		labelPane.setBackground(new Color(0,80,180));
		JPanel todayPane = new JPanel();
		today.setForeground(new Color(220,220,220));
		today.setFont(today.getFont().deriveFont(Font.BOLD));
		todayPane.setLayout(new BoxLayout(todayPane, BoxLayout.X_AXIS));
		todayPane.add(today);
		todayPane.setOpaque(false);
		JPanel PassPane = new JPanel();
		tPass.setForeground(new Color(220,220,220));
		tPass.setFont(tPass.getFont().deriveFont(Font.BOLD));
		PassPane.setLayout(new BoxLayout(PassPane, BoxLayout.X_AXIS));
		PassPane.add(tPass);
		PassPane.setOpaque(false);
		JPanel PicPane = new JPanel();
		PicPane.setLayout(new BoxLayout(PicPane, BoxLayout.X_AXIS));
		PicPane.setOpaque(false);
		PicPane.add(picture);
		labelPane.setOpaque(false);
		labelPane.add(todayPane);
		labelPane.add(PassPane);
		labelPane.add(PicPane);

		JPanel choicePane = new JPanel();
		choicePane.setBackground(new Color(0,80,180));
		nPass.setForeground(new Color(220,220,220));
		choicePane.setOpaque(false);
		choicePane.add(dayList);
		choicePane.add(newDate);
		choicePane.add(newYear);
		choicePane.add(nPass);
		
		add(labelPane, BorderLayout.PAGE_START);
		add(choicePane);
		
		setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
}
	
	public void paintComponent(Graphics g){
		int temp = Integer.valueOf(newDate.getText());
		int temp1 = Integer.valueOf(newYear.getText());
		nPass.setFont(nPass.getFont().deriveFont(Font.BOLD));
		nPass.setHorizontalAlignment(JLabel.CENTER);
		nPass.setText("New Password: " + 
				GetInfo.getPassword((dayList.getSelectedIndex() + 1), temp, temp1));
	}
	
	// Listens to the combo box
	public void actionPerformed(ActionEvent arg0) {		
		repaint();
	}
	
	protected static void updateLabel(String name) {
		ImageIcon icon = GetFile.createImageIcon(name);
		picture.setIcon(icon);
		picture.setToolTipText("Advantage Controls");
		if (icon != null) {
			picture.setText(null);
		} else {
			picture.setText("Image not found");
		}
	}
	
	/**
     * Create the GUI and show it.  For thread safety,
     * this method should be invoked from the
     * event-dispatching thread.
     */
    private static void createAndShowGUI() {
    	final int WINDOWSIZE_X = 300;
    	final int WINDOWSIZE_Y = 250;
    	//Create and setup the window.
    	JFrame.setDefaultLookAndFeelDecorated(false);
    	
    	//Create and setup the content pane
    	JComponent newContentPane = new Password();
    	frame.setContentPane(GetFile.wrapInBackgroundImage(newContentPane, 
    			GetFile.createImageIcon("bg.jpg")));

    	frame.addWindowListener(new WindowAdapter() {
    		public void windowClosing(WindowEvent we) { System.exit(0); }
    	});
    	//Display the window
    	frame.pack();
    	frame.setIconImage(GetFile.getIconImage("Adv-Icon.gif"));
    	frame.setBackground(new Color(0,80,180));
    	frame.setTitle("Today's Password: " + GetInfo.getPassword());
    	frame.setResizable(false);
    	frame.setSize(WINDOWSIZE_X, WINDOWSIZE_Y);
    	frame.setLocation(GetInfo.getHorizontalPosition(WINDOWSIZE_X), 
    			GetInfo.getVerticalPosition(WINDOWSIZE_Y));
    	frame.setVisible(true);
    }

    public static void main(String[] args){
    	//Schedule a job for the event-dispatching thread:
    	//creating and showing this application's GUI.
    	javax.swing.SwingUtilities.invokeLater(new Runnable() {
    		public void run() {
    			init();
    			createAndShowGUI();
    		}
    	});
    }
}