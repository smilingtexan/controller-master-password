/**
 * @author Timothy Carter
 *
 */
package com.password;

import javax.swing.*;
import java.awt.*;

public class GetFile extends JPanel {
	private static final long serialVersionUID = 2336242043196552619L;

	/**
	 * <code>createImageIcon</code> returns a path to an image file
	 * 	make sure picture is placed in the images/ subdirectory
	 * 
	 * @param file : file name of picture
	 * @return - ImageIcon
	 */
	public static ImageIcon createImageIcon(String file) {
		java.net.URL imgURL = GetFile.class.getResource("images/" + file);
		if (imgURL != null) {
			return new ImageIcon(imgURL);
		} else {
			System.err.println("Couldn't find file: \"images/" + file + "\"");
			return null;
		}
	}
	
	/**
	 * <code>getIconImage</code> fetches an image file for use as window (frame's) icon.
 	 * 	make sure picture is placed in the images/ subdirectory
	 *
	 * @param file
	 * @return
	 */
	public static Image getIconImage(String file) {
		java.net.URL imgURL = GetFile.class.getResource("images/" + file);
		Image image = new ImageIcon(imgURL).getImage();
		return image;
	}

	/**
	 * <code>GridBagConstraints</code>
	 * Setup the constraints so that the user supplied component and the background image
	 * label overlap and resize identically
	 * 
	 */
	private static final GridBagConstraints gbc;
	static {
		gbc = new GridBagConstraints();
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.weightx = 1.0;
		gbc.weighty = 1.0;
		gbc.fill = GridBagConstraints.BOTH;
		gbc.anchor = GridBagConstraints.NORTHWEST;
	}
	
	/**
	 * Wraps a Swing component in a background image
	 * Simply invokes the overloaded variant with Top/Leading alignment for background image
	 * @param component - to wrap the background image
	 * @param backgroundIcon - the background image (Icon)
	 * @return the wrapping JPanel 
	 */
	public static JPanel wrapInBackgroundImage(JComponent component, Icon backgroundIcon) {
		return wrapInBackgroundImage(component, backgroundIcon, JLabel.TOP, JLabel.LEADING);
	}
	
	/**
	 * Wraps a Swing component in a background image
	 * The vertical and horizontal alignment constrains the JLabel
	 * @param component - to wrap the background image
	 * @param backgroundIcon - the background image (Icon)
	 * @param verticalAlignment - vertical Alignment
	 * @param horizontalAlignment - horizontal Alignment
	 * @return the wrapping JPanel 
	 */
	public static JPanel wrapInBackgroundImage(JComponent component, Icon backgroundIcon, int verticalAlignment, int horizontalAlignment) {
		// make the passed in swing component transparent
		component.setOpaque(false);
		
		// create wrapper JPanel
		JPanel backgroundPanel = new JPanel(new GridBagLayout());
		
		// add the passed in swing component first to ensure it is in front
		backgroundPanel.add(component, gbc);
		
		// create a label to paint the background image
		JLabel backgroundImage = new JLabel(backgroundIcon);
		
		// set minimum and preferred sized so that the size of the image does no affect the layout size
		backgroundImage.setPreferredSize(new Dimension(1,1));
		backgroundImage.setMinimumSize(new Dimension(1,1));
		
		// align the image as specified
		backgroundImage.setVerticalAlignment(verticalAlignment);
		backgroundImage.setHorizontalAlignment(horizontalAlignment);
		
		// add the background label
		backgroundPanel.add(backgroundImage, gbc);
		
		// return the wrapper
		return backgroundPanel;
	}
}
