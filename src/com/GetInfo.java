/**
 * 
 */
package com.password;
import java.awt.*;
import java.util.Calendar; 

/**
 * @author Timothy Carter
 * @see Calendar
 * @see Dimension
 * @see Toolkit.getDefaultToolkit().getScreenSize()
 *
 */
public class GetInfo {

	static String[] dayValues = { "Sunday", "Monday", "Tuesday", 
			"Wednesday", "Thursday", "Friday", "Saturday" };
	static Calendar date = Calendar.getInstance();
	static Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

	/**
	 * <code>getDayValues</code> returns a String array with the text of each
	 * 		day of the week
	 * 
	 * @return - newDays; an array
	 */
	static public String[] getDayValues() {
		return dayValues;
	}
	
	/**
	 * <code>getDate</code> returns a string with the current day, date, month & year
	 * 
	 * @return - String in the format DAY, MONTH DD, YEAR
	 */
	static public String getDate() {
		String[] monthValues = { "January", "February", "March", "April",
			"May", "June", "July", "August", "September", "October",
			"November", "December" };
		
		return ("Today is: " + dayValues[(date.get(Calendar.DAY_OF_WEEK) - 1)] + ", " + monthValues[date.get(Calendar.MONTH)] + " "
			+ (date.get(Calendar.DATE)) + ", " + (date.get(Calendar.YEAR)));
	}

	/**
	 * <code>getDate</code> returns a string with the current date
	 *  
	 *  @param - var, true/false; provided as a means to overload the function
	 *  	if true, this function will return a String for the current date
	 *  	if false, this function will return a String for the current year
	 *  
	 * @return - String for the current date
	 */
	static public String getDate(boolean var) {
		if (var == false) {
			return (Integer.toString(date.get(Calendar.YEAR)));
		} else {
			return (Integer.toString(date.get(Calendar.DATE)));
		}
	}
	
	/**
	 * <code>getDayValue</code> retrieves the numerical value for the current day of
	 * 		the week, and then subtracts one - for use in selecting the element of an
	 * 		array.
	 * 
	 * @return - int, that is the value of the Day of the Week (0 = Sunday, 6 = Saturday)
	 */
	static public int getDayValue() {
		return (date.get(Calendar.DAY_OF_WEEK) - 1);
	}
	
	/**
	 * <code>getPassword</code> calculates the current MASTER password for
	 * 		today's date; converts the integer to a string & formats the string
	 * 		to add leading zeros if necessary to make 4 digits
	 * 
	 * @see Integer.toString()
	 * @see Calendar
	 * 
	 * @return - String, the master Password for today's date
	 */
	static public String getPassword() {
		int tempPass = 0;

		/* Code redacted for security */
		
		String Password = Integer.toString(tempPass);
		if (tempPass < 10)
			Password = ("0" + Password);
		if (tempPass < 100)
			Password = ("0" + Password);
		if (tempPass < 1000)
			Password = ("0" + Password);
		
		return Password;
	}
	/**
	 * <code>getPassword</code> calculates the current MASTER password for
	 * 		today's date; converts the integer to a string & formats the string
	 * 		to add leading zeros if necessary to make 4 digits
	 * 
	 * This function is overloaded to provide a means for calculating the password
	 * with other variable besides the current day / date.
	 * 
	 * @see Integer.toString()
	 * @see Calendar
	 * 
	 * @param - newDay - new Day value to use in password calculation (Sun = 1; Sat = 7)
	 * @param - newDate - new Date value to use in password calculation
	 * @param - newYear - new Year value to use in password calculation
	 * @return - String, the master Password for today's date
	 */
	static public String getPassword(int newDay, int newDate, int newYear) {
		int tempPass = 0;

		/* Code redacted for security */
		
		String Password = Integer.toString(tempPass);
		if (tempPass < 10)
			Password = ("0" + Password);
		if (tempPass < 100)
			Password = ("0" + Password);
		if (tempPass < 1000)
			Password = ("0" + Password);
		
		return Password;
	}
	
	/**
	 * <code>getHorizontalPosition</code> gets screenSize info and returns an INT
	 * 		that is the screen width - horizontalSize and then divided by 2
	 * 
	 * @param horizontalSize (of the frame)
	 * @return
	 */
	static public int getHorizontalPosition(int horizontalSize) {
		return Math.max(0, (screenSize.width - horizontalSize) / 2);
	}
	
	/**
	 * <code>getVerticalPosition</code> gets screenSize info and returns an INT
	 * 		that is the screen height - verticalSize and then divided by 2
	 * 
	 * @param verticalSize (of the frame)
	 * @return
	 */
	static public int getVerticalPosition(int verticalSize) {
		return Math.max(0, (screenSize.height - verticalSize) / 2);
	}
}
