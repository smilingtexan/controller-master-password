# Master password generator
## by: Timothy Carter (aka: SmilingTexan)

This is a project I did when I was at [Advantage Controls](http://www.advantagecontrols.com).
It is designed to calculate the master password for their controllers.

I've redacted the actual password calculation, just in case they are still using
the same formula, as I don't want to give away their trade secrets. (Sorry, if that's
what you were looking for, you need to look elsewhere.)

### **Use at your own risk.**

The code is a beginner's look at Java. Since this is one of the first programs I ever developed when I started learning Java.

It's posted here as archive, as well as to help future developers to learn the Java language.
